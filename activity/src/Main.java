import static java.lang.System.out;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        out.println("Enter first name: ");
        String fName= sc.nextLine();

        out.println("Enter last name: ");
        String lName= sc.nextLine();

        out.println("First subject grade:");
        double num1 = sc.nextDouble();

        out.println("Second subject grade: ");
        double num2 = sc.nextDouble();

        out.println("Third subject grade: ");
        double num3 = sc.nextDouble();

        double sum = (num1 + num2 + num3) / 3;

        out.println("Good day, " + fName + " " + lName);
        out.println("Your grade average is: " + sum);
    }
}