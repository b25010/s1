package com.zuitt.example;
import static java.lang.System.out;

public class Variables {

    public static void main(String[] args) {
        out.println("Zawarudo!!");

        int age = 0;
        char middleName = 0;

        // Variable Declaration vs Initialization

        int x;
        int y = 0;

        // Initialization after declaration
        x = 1;

        // Output to the system
        out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data Types
        // predefined w/n the JAVA programming language w/c is used for single-valued variable with limited capabilities.

        //  int - whole number values
        int wholeNumber = 100;
        out.println(wholeNumber);

        // long - L is being added at the end of the long number to be recognized.
        long worldPopulation = 78628811457878L;
        out.println(worldPopulation);

        // float - and f at the end if the float to be recognized.
        float piFloat = 3.14159265359f;
        out.println(piFloat);

        // double - floating point values
        double piDouble = 3.14159265359;
        out.println(piDouble);

        // char - stores single character and uses single quote on initialization.
        char letter = 'a';
        out.println(letter);

        // boolean - true or false
        boolean isLove = true;
        boolean isTaken = false;

        out.println(isLove);
        out.println(isTaken);

        // constants
        // Java uses the "final" keyword so the variable's value cannot be changed.
        final int PRINCIPAL = 3000;
        out.println(PRINCIPAL);
        // PRINCIPAL = 10; --> ERROR!!

        // end: Non-primitive data

        // String - stores a sequence or array of characters
            // - are actually object than can use methods.

        String uName = "JSmith";
        out.println(uName);

        // sample string method
        int stringLength  = uName.length();
        out.println(stringLength);

    }
}
